import matplotlib.pyplot as plt
from skimage import io,data,transform
from skimage.color import rgb2lab, lab2rgb
from sklearn.neighbors import KNeighborsRegressor
import numpy as np
import random
import os
import math

data_dir = "naive_data/"
block_size = 1

def SplitImage(img, size = block_size):
    w, h = img.shape[:2]
    length = (2 * size + 1) * (2 * size + 1)
    result = []
    for x in range(size, w - size):
        for y in range(size , h - size):
            result.append(img[x - size : x + size + 1, y - size : y + size + 1, 0].reshape(-1))
    return result

def ReadImage(filename, size = block_size):
    img= rgb2lab(io.imread(filename))
    w, h = img.shape[:2]
    length = (2 * size + 1) * (2 * size + 1)
    X = []
    Y = []
    for x in range(size, w - size):
        for y in range(size , h - size):
            #print(x,y)
            X.append(img[x - size : x + size + 1, y - size : y + size + 1, 0].reshape(-1))
            Y.append(img[x,y,1:3])
    return X,Y

def CreateDataSet(data_dir = data_dir,size = 1):
    X = []
    Y = []
    n = 0
    for file in os.listdir(data_dir):
        print("reading: ",file)
        X0, Y0 = ReadImage(os.path.join(data_dir, file))
        X.extend(X0)
        Y.extend(Y0)
        n+=1
        if (n >= size):
            break
    return X,Y

#print("reading data")
#X,Y = CreateDataSet(size = 100)
X,Y = ReadImage("vincent.jpeg")
#print("finish reading")

nbrs = KNeighborsRegressor(n_neighbors=5, weights = 'distance')

#print("start fitting")
nbrs.fit(X,Y)
#print("finish fitting")


def Rebuild(filename, size = block_size):
    img_in = io.imread(filename)
    if len(img_in.shape)<3:
        new_img = np.zeros([img_in.shape[0], img_in.shape[1], 3])
        new_img[:,:,0] = img_in * (100.0/256)
        img = new_img
    else:
        img = rgb2lab(img_in)

    w0,h0 = img.shape[:2]
    img_resize = transform.resize(img,(256,256))
    w, h = img_resize.shape[:2]
    #print(w,h)
    length = (2 * size + 1) * (2 * size + 1)
    photo = np.zeros([w,h,3])
    inputData = SplitImage(img_resize)
    print("predicting")
    p_ab = nbrs.predict(inputData).reshape((w-2*size),(h-2*size),-1)
    print("predicting")
    for x in range(size, w - size):
        for y in range(size , h - size):
            #photo[x, y, 0] = img_resize[x, y, 0]
            photo[x, y, 1] = p_ab[x - size, y - size, 0]
            photo[x, y, 2] = p_ab[x - size, y - size, 1]
    photo = transform.resize(photo,(w0,h0))
    photo[:,:,0] = img[:,:,0]
    return photo

def color_it(filein, fileout):
    new_photo = Rebuild(filein)
    io.imsave(fileout, lab2rgb(new_photo))

