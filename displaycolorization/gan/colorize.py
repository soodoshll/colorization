import numpy as np
import pprint

import os
import sys
import time
import math
import pickle

import skimage.transform
from skimage.color import rgb2lab, lab2rgb

from scipy import misc
from glob import glob

from ops_new import *
from utils_new import *

#from .model_wgan import WGAN

import tensorflow as tf

CONFIG = {"devices":"cpu:0",
          "model_dir":"./gan/model",
          "batch_size":64,
          "improved_wgan":True,
          "gradient_penalty_lambda":10,
          "d_kernel_size":5,
          "color_space":"YUV",
          "image_size":64,
          "z_dim":100
          }


class Colorize(object):
    def __init__(self, sess, config=None):
        self.sess = sess
        self.config = config
        self.build_model(config)

    def load_image_from_file(self, filename, config=None):
        one_data = misc.imread(filename)
        print(len(one_data.shape))
        if (len(one_data.shape)) < 3:
            new_data = np.zeros([one_data.shape[0], one_data.shape[1],3])
            new_data[:,:,0] = one_data
            one_data = new_data
        elif config['color_space']=="YUV":
            one_data = cvtRGB2YUV(one_data)
        return np.array(one_data, dtype = np.float32)/127.5 - 1.

    def colorize(self, filename, outfile, config=None):
        save_size = int(math.sqrt(config['batch_size']))
        #images = data.load_data(config)
        image_in =  self.load_image_from_file(filename, config)
        image_resized = skimage.transform.resize(image_in,(64,64))
        image_batch = np.zeros((64,64,64,3))
        image_batch[:,:,:,:] = image_resized
        sample_z = np.random.uniform(-1, 1, size=(64, config['z_dim']))
        _generate_image, _g_loss, _d_loss = self.sess.run([self.generate_image, self.g_loss, self.d_loss], feed_dict={self.z: sample_z, self.images: image_batch})
        image_out = skimage.transform.resize(_generate_image[0], image_in.shape)
        image_out[:,:,0] = image_in[:,:,0]
        save_image(image_out, outfile, color_space=config['color_space'])

    def build_model(self, config):
        #data
        self.z = tf.placeholder(tf.float32, [config['batch_size'], config['z_dim']], name='z')
        self.images = tf.placeholder(tf.float32, [config['batch_size']] + [config['image_size'], config['image_size'], 3], name='real_images')
        
        #generator
        if config['color_space']=="YUV":
            self.images_Y, self.images_U, self.images_V = tf.split(self.images, 3, 3)
            self.generate_image_UV = self.generator_colorization(self.z, self.images_Y, config=config)
            self.generate_image = tf.concat([self.images_Y, self.generate_image_UV], 3)
        elif config['color_space']=="RGB":
            self.generate_image = self.generator_colorization(self.z, config=config)

        #discriminator_wgan # on sigmoid = no prob
        self.logits_real = self.discriminator_wgan(self.images, config=config)
        self.logits_fake = self.discriminator_wgan(self.generate_image, reuse=True, config=config)

        #w-distance
        self.g_loss = -tf.reduce_mean(self.logits_fake)
        self.d_loss = -tf.reduce_mean(self.logits_real - self.logits_fake)

        #improved wgan
        if config['improved_wgan']:
            alpha = tf.random_uniform(
                shape=[config['batch_size'], 1],
                minval=0.,
                maxval=1.,
                dtype=tf.float32
                )
            differences = self.generate_image - self.images
            interpolates = self.images+(alpha*differences)
            gradients = tf.gradients(self.discriminator_wgan(interpolates, reuse=True, config=config), [interpolates])[0]
            slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1]))
            self.gradient_penalty = tf.reduce_mean((slopes-1.)**2)
            self.d_loss+=config['gradient_penalty_lambda']*self.gradient_penalty

        self.total_loss = self.d_loss + self.g_loss

        t_vars = tf.trainable_variables()
        self.d_vars = [var for var in t_vars if 'd_' in var.name]
        self.g_vars = [var for var in t_vars if 'g_' in var.name]

        self.saver = tf.train.Saver()

    def load(self, config=None):
        checkpoint_dir = config['model_dir']
        print('checkpoint_dir:', checkpoint_dir)
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)  #get_checkpoint_state() returns CheckpointState Proto
        if ckpt and ckpt.model_checkpoint_path:
            latest_checkpoint = tf.train.latest_checkpoint(checkpoint_dir)
            print('latest checkpoint:', latest_checkpoint)
            ckpt_name = os.path.basename(ckpt.model_checkpoint_path)
            #ckpt_name = 'WGAN.model-77922'
            print('loading checkpoint:', os.path.join(checkpoint_dir, ckpt_name))
            self.saver.restore(self.sess, os.path.join(checkpoint_dir, ckpt_name))
            return True
        else:
            #print "www"
            return False

    def generator_colorization(self, z, image_Y, config=None):
        with tf.variable_scope("generator") as scope:
            # project z
            h0 = linear(z, config['image_size'] * config['image_size'], 'g_h0_lin', with_w=False)
            # reshape 
            h0 = tf.reshape(h0, [-1, config['image_size'], config['image_size'], 1])
            h0 = tf.nn.relu(batch_norm(h0, name = 'g_bn0'))
            # concat with Y
            h1 = tf.concat([image_Y, h0], 3)
            #print 'h0 shape after concat:', h0.get_shape()
            h1 = conv2d(h1, 128, k_h = 7, k_w = 7, d_h = 1, d_w = 1, name = 'g_h1_conv')
            h1 = tf.nn.relu(batch_norm(h1, name = 'g_bn1'))

            h2 = tf.concat([image_Y, h1], 3)
            h2 = conv2d(h2, 64, k_h = 5, k_w = 5, d_h = 1, d_w = 1, name = 'g_h2_conv')
            h2 = tf.nn.relu(batch_norm(h2, name = 'g_bn2'))
            
            h3 = tf.concat([image_Y, h2], 3)
            h3 = conv2d(h3, 64, k_h = 5, k_w = 5, d_h = 1, d_w = 1, name = 'g_h3_conv')
            h3 = tf.nn.relu(batch_norm(h3, name = 'g_bn3'))

            h4 = tf.concat([image_Y, h3], 3)
            h4 = conv2d(h4, 64, k_h = 5, k_w = 5,  d_h = 1, d_w = 1, name = 'g_h4_conv')
            h4 = tf.nn.relu(batch_norm(h4, name = 'g_bn4'))

            h5 = tf.concat([image_Y, h4], 3)
            h5 = conv2d(h5, 32, k_h = 5, k_w = 5,  d_h =1, d_w = 1, name = 'g_h5_conv')
            h5 = tf.nn.relu(batch_norm(h5, name = 'g_bn5'))
            
            h6 = tf.concat([image_Y, h5], 3)
            h6 = conv2d(h6, 2, k_h = 5, k_w = 5,  d_h = 1, d_w = 1, name = 'g_h6_conv')
            out = tf.nn.tanh(h6)

            print('generator out shape:', out.get_shape())

            return out

    def discriminator_wgan(self, image, y=None, reuse=False, config=None):
        with tf.variable_scope("discriminator") as scope:
            if reuse:
                scope.reuse_variables()

            h0 = lrelu(conv2d(image, 64, k_h=config['d_kernel_size'], k_w=config['d_kernel_size'], name='d_h0_conv'), name='d_bn0')
            h1 = lrelu(batch_norm(conv2d(h0, 128, k_h=config['d_kernel_size'], k_w=config['d_kernel_size'], name='d_h1_conv'), name='d_bn1'))
            h2 = lrelu(batch_norm(conv2d(h1, 256, k_h=config['d_kernel_size'], k_w=config['d_kernel_size'], name='d_h2_conv'), name='d_bn2'))
            h3 = lrelu(batch_norm(conv2d(h2, 512, k_h=config['d_kernel_size'], k_w=config['d_kernel_size'], name='d_h3_conv'), name='d_bn3'))

            h4 = linear(tf.reshape(h3, [config['batch_size'], -1]), 64, name = 'd_h4_lin')
            h5 = linear(h4, 1, 'd_h5_lin')
            #wgan without sigmoid
            return h5
def color_it(input_file, output_file):
    print(sys.argv[0])
    config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
    config.gpu_options.allow_growth = True

    with tf.Session(config=config) as sess:
        with tf.device(CONFIG['devices']):
            dcgan = Colorize(sess, config=CONFIG)
            if dcgan.load(CONFIG):
                print(" [*] Load SUCCESS")
                #dcgan.colorize("./img/test.jpg","./result/gan.jpg",CONFIG)
                dcgan.colorize(input_file,output_file,CONFIG)
            else:
                print(" [!] Load failed...")

if __name__ == '__main__':
    color_it(sys.argv[1],sys.argv[2])
#     tf.app.run()


