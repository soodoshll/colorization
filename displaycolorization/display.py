import sys
#sys.path.append('./naivecnn')
#sys.path.append('./gan')
#sys.path.append('./cnn')

import caffe
# import keras
import os

import time
import forfun
import knnfun
#import naivecnn.colorize
#import gan.colorize
#import cnn.colorize

from flask import Flask, request, redirect, url_for, send_from_directory, render_template
from werkzeug import secure_filename

UPLOAD_FOLDER = 'img'
RESULT_FOLDER = 'result'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
PYTHON_COMMAND = 'python3'
PYTHON_COMMAND2 = 'python2'
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['RESULT_FOLDER'] = RESULT_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filename = time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime()) + os.path.splitext(filename)[1]
            file.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
            #make_picture(filename)
            return redirect(url_for('comparison',
                                    filename=filename))
    return render_template('upload.html')

@app.route('/make_picture/<filename>/<model>', methods=['GET', 'POST'])
def make_picture(filename,model):

    if model == '1':
        # print(1)
        os.system(PYTHON_COMMAND+" ./naivecnn/colorize.py ./img/"+filename+" ./result/naivecnn_"+filename)
    elif model == '2':
        # print(2)
        os.system(PYTHON_COMMAND+" ./gan/colorize.py ./img/"+filename+" ./result/gan_"+filename)
    elif model == '3':
        # print(3)
        os.system(PYTHON_COMMAND2+" ./cnn/colorize.py -img_in ./img/"+filename+" -img_out ./result/cnn_v1_"+filename+" --prototxt ./cnn/models/colorization_deploy_v1.prototxt --caffemodel ./cnn/models/colorization_release_v1.caffemodel")
    elif model == '4':
        # print(4)
        os.system(PYTHON_COMMAND2+" ./cnn/colorize.py -img_in ./img/"+filename+" -img_out ./result/cnn_v2_"+filename+" --prototxt ./cnn/models/colorization_deploy_v2.prototxt --caffemodel ./cnn/models/colorization_release_v2_norebal.caffemodel")
    elif model == '5':
        # print(5)
        os.system(PYTHON_COMMAND2+" ./cnn/colorize.py -img_in ./img/"+filename+" -img_out ./result/cnn_v3_"+filename+" --prototxt ./cnn/models/colorization_deploy_v2.prototxt --caffemodel ./cnn/models/colorization_release_v2.caffemodel")
    elif model =='6':
        # print(6)
        forfun.color_it("./img/"+filename,"./result/fun_"+filename)
    elif model =='7':
        # print(7)
        knnfun.color_it("./img/"+filename,"./result/knn_"+filename)
    elif model =='8':
        os.system("th colorize.lua ./img/"+filename+" ./result/lizuka_"+filename)
    return ""
    
@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

@app.route('/result/<filename>')
def result_naivecnn(filename):
    return send_from_directory(app.config['RESULT_FOLDER'], 'naivecnn_'+filename)

@app.route('/result_gan/<filename>')
def result_gan(filename):
    return send_from_directory(app.config['RESULT_FOLDER'], 'gan_'+filename)

@app.route('/result_v1/<filename>')
def result_v1(filename):
    return send_from_directory(app.config['RESULT_FOLDER'], 'cnn_v1_'+filename)

@app.route('/result_v2/<filename>')
def result_v2(filename):
    return send_from_directory(app.config['RESULT_FOLDER'], 'cnn_v2_'+filename)

@app.route('/result_v3/<filename>')
def result_v3(filename):
    return send_from_directory(app.config['RESULT_FOLDER'], 'cnn_v3_'+filename)

@app.route('/fun/<filename>')
def fun(filename):
    return send_from_directory(app.config['RESULT_FOLDER'], 'fun_'+filename)

@app.route('/knn/<filename>')
def knn(filename):
    return send_from_directory(app.config['RESULT_FOLDER'], 'knn_'+filename)

@app.route('/lizuka/<filename>')
def lizuka(filename):
    return send_from_directory(app.config['RESULT_FOLDER'], 'lizuka_'+filename)

@app.route('/mix/<filename>')
def mix(filename):
    return render_template("mix.html")

@app.route('/comparison/<filename>')
def comparison(filename):
    return render_template('comparison.html',filename = filename)

if __name__ == '__main__':
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"] = ""
    app.run(debug = True)
