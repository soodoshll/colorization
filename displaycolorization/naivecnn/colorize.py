import sys
import skimage
import skimage.transform
from skimage.io import imread, imsave
from skimage.color import rgb2gray, gray2rgb, rgb2lab, lab2rgb
import numpy as np
import glob
import os
from PIL import Image

def ResizeImage(filein, fileout, width, height, type):
    img = Image.open(filein)
    out = img.resize((width, height),Image.ANTIALIAS) #resize image with high-quality
    out.save(fileout, type)

def color_it(input_file, output_file):
    import tensorflow as tf
    import keras
    from keras.models import Sequential
    from keras.layers import Conv2D, UpSampling2D, InputLayer, Conv2DTranspose
    from keras.preprocessing.image import img_to_array, load_img
    
    #import os


    keras.backend.set_session(tf.Session(config=tf.ConfigProto(device_count={'gpu':0})))

    model = Sequential()
    model.add(InputLayer(input_shape=(None, None, 1)))
    model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
    model.add(Conv2D(64, (3, 3), activation='relu', padding='same', strides=2))
    model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
    model.add(Conv2D(128, (3, 3), activation='relu', padding='same', strides=2))
    model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
    model.add(Conv2D(256, (3, 3), activation='relu', padding='same', strides=2))
    model.add(Conv2D(512, (3, 3), activation='relu', padding='same'))
    model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
    model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
    model.add(UpSampling2D((2, 2)))
    model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
    model.add(UpSampling2D((2, 2)))
    model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
    model.add(Conv2D(2, (3, 3), activation='tanh', padding='same'))
    model.add(UpSampling2D((2, 2)))
    # Finish model
    model.compile(optimizer='rmsprop', loss='mse')
    
    model.load_weights('simple_model.h5')

    # Load black and white images
    color_me = []
    image_in = Image.open(input_file)
    image_shape = image_in.size
    ResizeImage(input_file, output_file, image_shape[0] / 8 * 8, image_shape[1] / 8 * 8, 'JPEG')
    image_l = img_to_array(load_img(output_file))
    image_l = np.array(image_l, dtype=float)
    image_l = rgb2lab(1.0/255*image_l)
    color_me.append(img_to_array(load_img(output_file)))
    color_me = np.array(color_me, dtype=float)
    color_me = rgb2lab(1.0/255*color_me)[:,:,:,0]
    color_me = color_me.reshape(color_me.shape+(1,))

    # Test model
    output = model.predict(color_me)
    output = output * 128

    # Output colorizations
    cur = np.zeros((image_shape[1] / 8 * 8, image_shape[0] / 8 * 8, 3))
    cur[:,:,0] = color_me[0][:,:,0]
    cur[:,:,1:] = output[0]
    imsave(output_file, lab2rgb(cur))
    # ResizeImage(output_file, output_file, image_shape[0], image_shape[1], 'JPEG')
    # image_out = img_to_array(load_img(output_file))
    # image_out = np.array(image_out, dtype=float)
    # image_out = rgb2lab(1.0/255*image_out)
    # image_out[:,:,0] = image_l[:,:,0]
    # imsave(output_file, lab2rgb(image_out))
    keras.backend.clear_session()

if __name__ == '__main__':
    color_it(sys.argv[1],sys.argv[2])
