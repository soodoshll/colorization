# 模型介绍

## JustForFun

用Laplace算子对图像进行边缘检测，然后用bfs算法对图片进行随机上色，在灰度变化越明显的位置对应地加入更大的随机波动。

## KNN模型

取一张图像作为样例，把一个像素点周围$3 \times 3$的灰度值方块作为输入，ab通道的色彩作为输出，找到欧几里得距离下样例中与输入最近的k个点，以距离的倒数为权值加权平均得到颜色。染色后的图片带有样例图像的特征。

## CNN模型  

### Euclidean Loss

在CNN网络中，输入一张图的灰度通道$X \in \mathbb{R}^{H \times W \times 1}$，输出其ab通道$\widehat{Y} \in \mathbb{R}^{H \times W \times 2}$，从而生成彩色图片，将欧几里得距离作为损失函数:
$$L_2(\widehat{Y}, Y) = \frac{1}{2} \sum_{h, w} \parallel Y_{h, w} - \widehat{Y}_{h, w} \parallel_2^2,$$
其中$Y$是图片真实的ab通道。

由于一张灰度图可能对应多种彩色图，欧几里得距离损失函数下的最优解倾向于灰褐色，色彩饱和度很低。

### Classification Loss

将颜色空间离散化，把ab通道的取值分为313类，将分类损失函数作为目标函数。在CNN网络中，输入一张图的灰度通道$X$，输出其每个像素点在313类ab值上的概率分布$\widehat{Z} \in [0, 1]^{H \times W \times 313}$，再将$\widehat{Z}$映射到一个ab值$\widehat{Y}$。损失函数定义为多项交叉熵损失：
$$L_{cl}(\widehat{Z}, Z) = - \sum_{h, w, q} Z_{h, w, q} log(\widehat{Z}_{h, w, q}).$$

虽然此模型较之欧几里得损失的CNN模型有显著提升，但仍存在色彩饱和度较低的问题。

### Classification Loss + Rebalance

在分类损失函数的基础上，为了克服色彩饱和度低的问题，根据颜色的稀有程度对不同颜色进行再平衡，鼓励更鲜艳的颜色出现。具体来说，损失函数改进为
$$L_{cl}(\widehat{Z}, Z) = - \sum_{h, w} v(Z_{h, w}) \sum_q Z_{h, w, q} log(\widehat{Z}_{h, w, q}),$$
其中
$$v(Z_{h, w}) = w_{q^*},  \ \ q^* = \arg \max_{q} Z_{h, w, q},$$
$$w \varpropto  \left( (1 - \lambda) \widetilde{p} + \frac{\lambda}{Q} \right)^{-1}, \ \ \mathbb{E}[w] = \sum_q \widetilde{p}_q w_q = 1.$$

此模型较之前两个CNN模型，给图片上色更加鲜艳真实。

![richzhang](./cnn_richzhang.jpg)

### Global Priors + Local Features

设计了一种可以融合局部和全局信息的卷积神经网络。该模型有四个主要的部分组成：低层次特征网络、中层次特征网络、全局特征网络和上色网络。首先，低层次特征从图片中抽取出来，利用这些信息计算出全局特征和中层次特征，然后中层次特征和全局信息在融合层被融合，并作为输入进入上色网络，最终输出彩色图片。

![lizuka](./cnn_lizuka.png)

## GAN模型

利用conditional GAN为灰度图生成高保真度的彩色图像。该模型有两个对抗的部分组成：生成模型G和识别模型D。G将随机输入的噪音映射到一个接近真实的色彩分布，而G试图将生成的图片与真实图片区分开。通过精心设计G和D，在G和D不断对抗的过程中，生成的图片会越来越接近真实图片。通过控制输入噪声，用同一幅灰度图可以得到很多不同的高保真度的彩色图片。

![gan](./GAN.png)