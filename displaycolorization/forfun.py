# A naive version of diverse colorization
import matplotlib.pyplot as plt
from skimage import io,data,transform,filters
from skimage.color import rgb2lab, lab2rgb
import numpy as np
import random
import os
import math
import bisect
data_dir = ".\\naive_data\\"
fre = np.zeros((101, 256, 256))
"""


cor2 = np.array([[1,2,1],
                [0,0,0],
                [-1,-2,-1]])
"""


r = 16.0
cor = np.array([[0,-r/4,0],
                [-r/4,r,-r/4],
                [0,-r/4,0]])

def lab2grey(img):
    img[:,:,1] = 0
    img[:,:,2] = 0
    return img

def conv(img,xc,yc):
    v = 0
    for dx in range(-1,2):
        for dy in range(-1,2):
            x = xc + dx
            y = yc + dy
            if x >= 0 and x < img.shape[0] and y >= 0 and y < img.shape[1]:
                v += img[x,y] * cor[dx+1, dy+1]
    return v

def gen_edge(img):
    result = np.zeros((img.shape[0], img.shape[1]))
    for x in range(img.shape[0]):
        for y in range(img.shape[1]):
            result[x,y] = abs(conv(img,x,y)) / (2.0*r)
    return result

def foo(x):
    return -15*math.cos(x*math.pi)/2+0.5

def bfs_draw(img, sx, sy):
    w = img.shape[0]
    h = img.shape[1]
    edge = gen_edge(img)
    #edge = filters.roberts(img)
    #print(edge)
    #io.imshow(edge)
    #io.show()
    photo = np.zeros((w,h,3))
    init_a = random.uniform(-32,32)
    init_b = random.uniform(-32,32)
    start = [edge[sx,sy],sx,sy,init_a,init_b]
    q = [start]
    visited = np.zeros((w,h))
    maxv = max(init_a, init_b)
    while (q):
        cur = q.pop(0)
        xc = cur[1]
        yc = cur[2]
        photo[xc, yc, 0] = img[xc,yc] * 100
        photo[xc, yc, 1] = cur[3]
        photo[xc, yc, 2] = cur[4]
        visited[xc, yc] = 1
        for dx in range(-1,2):
            for dy in range(-1,2):
                x = xc + dx
                y = yc + dy
                if x >= 0 and x < w and y >= 0 and y < h and not visited[x, y]:
                    #print(edge[x,y])
                    new_a = cur[3] + random.uniform(-1,1)*foo(edge[x, y])
                    new_b = cur[4] + random.uniform(-1,1)*foo(edge[x, y])
                    new_node = [edge[x,y], x, y, new_a, new_b]
                    bisect.insort(q,new_node)
                    visited[x,y] = 1
    for x in range(w):
        for y in range(h):
            maxv = max(maxv, photo[x,y,1])
            maxv = max(maxv, photo[x,y,2])
    for x in range(w):
        for y in range(h):
            photo[x,y,1] = photo[x,y,1] / maxv * 128
            photo[x,y,2] = photo[x,y,2] / maxv * 128
    return photo


def color_it(filein, fileout):
    img_in= io.imread(filein)
    if len(img_in.shape) < 3:
        img_in = img_in * (1.0/256)
    else :
        img_in = io.imread(filein, as_grey = True)
    img_l = img_in.copy()
    w = img_in.shape[0]
    h = img_in.shape[1]
    img_in = transform.resize(img_in,(128,128))
    new_photo = transform.resize(bfs_draw(img_in,64,64),(w,h))
    new_photo[:,:,0] = img_l * 100
    io.imsave(fileout, lab2rgb(new_photo))